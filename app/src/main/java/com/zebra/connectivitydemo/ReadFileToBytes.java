package com.zebra.connectivitydemo;

public class ReadFileToBytes {
    public byte[] convertToByte (){
        String strg = "CT~~CD,~CC^~CT~\n" +
                "^XA\n" +
                "~TA000\n" +
                "~JSN\n" +
                "^LT0\n" +
                "^MNW\n" +
                "^MTT\n" +
                "^PON\n" +
                "^PMN\n" +
                "^LH0,0\n" +
                "^JMA\n" +
                "^PR6,6\n" +
                "~SD15\n" +
                "^JUS\n" +
                "^LRN\n" +
                "^CI27\n" +
                "^PA0,1,1,0\n" +
                "^XZ\n" +
                "^XA\n" +
                "^MMT\n" +
                "^PW637\n" +
                "^LL6337\n" +
                "^LS0\n" +
                "^FO91,6067^GFA,689,9000,60,:Z64:eJzt1jtuGzEQBuAhWLDkDcybLC+2WFFQkdI3CphKZa7AIAcI09EArck/XMlw0ixduciwEEaQPiwfM5wl0vEfjZiok+OfzAmhz8yFGoVMt2PLxfLdQj3sqVpOR9RwdZyGPXPF82BNM9wc5yNruXnOXuyF224tbPd4/rENXN5sFOua5R6u9ci63z18qcMiHDaHZustXI5t6dFWj7UtCOmUyadTc3WbtGbYVSw2KBA2qkzZvMKGRNQQil3oddL6vEXThkVohn1pRB+wX0kmvplfmVb6IdZfy4y94JiSbNhmv2dk6Bm2T+QGQLjCZlsQOthGRqzhIzqspEd2wz5nM6xpE/kM4LlFJFOWELaKxQqOc/Jun/FUCX22w7o6t1dI6Sf8NyF8gi1ifZk7I/MicwxiF491iw05yo4d2n19w0buu430IUt3m+X7NmdXpNRia5QQ1g/bZyyKx5/rJlZK6m5l1lMWO3py5S+Lg5qxuDeuBecbJSQ55ZFUM/fGuOsK8mqTEBblaHBJ3ibuq/2eRD7/aydycr+fPadNwnd2ogYffQEnSqMvDOv5NlFHez8yXezoRw8704906Pj0wd/w/oO3kRTQLhHFuqIL+4Te61NoCxEq0nf8KmUY6wkffCmjrNSqVatWrVq1atWqVatWrVq1atWqVatWrVq1n2v/AFDPZSw=:C258\n" +
                "^FT91,6257^A0N,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT271,6257^A0N,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT433,6257^A0N,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^FT91,6041^A0N,28,41^FH\\^CI28^FDGIG^FS^CI27\n" +
                "^FT199,6041^A0N,28,41^FH\\^CI28^FDGUZMAN /DANIELMR^FS^CI27\n" +
                "^FO91,5779^GFA,689,9000,60,:Z64:eJzt1jtuGzEQBuAhWLDkDcybLC+2WFFQkdI3CphKZa7AIAcI09EArck/XMlw0ixduciwEEaQPiwfM5wl0vEfjZiok+OfzAmhz8yFGoVMt2PLxfLdQj3sqVpOR9RwdZyGPXPF82BNM9wc5yNruXnOXuyF224tbPd4/rENXN5sFOua5R6u9ci63z18qcMiHDaHZustXI5t6dFWj7UtCOmUyadTc3WbtGbYVSw2KBA2qkzZvMKGRNQQil3oddL6vEXThkVohn1pRB+wX0kmvplfmVb6IdZfy4y94JiSbNhmv2dk6Bm2T+QGQLjCZlsQOthGRqzhIzqspEd2wz5nM6xpE/kM4LlFJFOWELaKxQqOc/Jun/FUCX22w7o6t1dI6Sf8NyF8gi1ifZk7I/MicwxiF491iw05yo4d2n19w0buu430IUt3m+X7NmdXpNRia5QQ1g/bZyyKx5/rJlZK6m5l1lMWO3py5S+Lg5qxuDeuBecbJSQ55ZFUM/fGuOsK8mqTEBblaHBJ3ibuq/2eRD7/aydycr+fPadNwnd2ogYffQEnSqMvDOv5NlFHez8yXezoRw8704906Pj0wd/w/oO3kRTQLhHFuqIL+4Te61NoCxEq0nf8KmUY6wkffCmjrNSqVatWrVq1atWqVatWrVq1atWqVatWrVq1n2v/AFDPZSw=:C258\n" +
                "^FT91,5969^A0N,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT271,5969^A0N,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT433,5969^A0N,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^FT91,5753^A0N,28,41^FH\\^CI28^FDGIG^FS^CI27\n" +
                "^FT199,5753^A0N,28,41^FH\\^CI28^FDGUZMAN /DANIELMR^FS^CI27\n" +
                "^FO91,5491^GFA,689,9000,60,:Z64:eJzt1jtuGzEQBuAhWLDkDcybLC+2WFFQkdI3CphKZa7AIAcI09EArck/XMlw0ixduciwEEaQPiwfM5wl0vEfjZiok+OfzAmhz8yFGoVMt2PLxfLdQj3sqVpOR9RwdZyGPXPF82BNM9wc5yNruXnOXuyF224tbPd4/rENXN5sFOua5R6u9ci63z18qcMiHDaHZustXI5t6dFWj7UtCOmUyadTc3WbtGbYVSw2KBA2qkzZvMKGRNQQil3oddL6vEXThkVohn1pRB+wX0kmvplfmVb6IdZfy4y94JiSbNhmv2dk6Bm2T+QGQLjCZlsQOthGRqzhIzqspEd2wz5nM6xpE/kM4LlFJFOWELaKxQqOc/Jun/FUCX22w7o6t1dI6Sf8NyF8gi1ifZk7I/MicwxiF491iw05yo4d2n19w0buu430IUt3m+X7NmdXpNRia5QQ1g/bZyyKx5/rJlZK6m5l1lMWO3py5S+Lg5qxuDeuBecbJSQ55ZFUM/fGuOsK8mqTEBblaHBJ3ibuq/2eRD7/aydycr+fPadNwnd2ogYffQEnSqMvDOv5NlFHez8yXezoRw8704906Pj0wd/w/oO3kRTQLhHFuqIL+4Te61NoCxEq0nf8KmUY6wkffCmjrNSqVatWrVq1atWqVatWrVq1atWqVatWrVq1n2v/AFDPZSw=:C258\n" +
                "^FT91,5681^A0N,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT271,5681^A0N,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT433,5681^A0N,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^FT91,5465^A0N,28,41^FH\\^CI28^FDGIG^FS^CI27\n" +
                "^FT199,5465^A0N,28,41^FH\\^CI28^FDGUZMAN /DANIELMR^FS^CI27\n" +
                "^FO91,4568^GFA,233,31260,60,:Z64:eJzty8EJwlAURNERFy5Twu8kaUwwYGOxk5Twl1kIz9eHZzNcGE595r2uW+0ja7q2+czjWPZUz7jWZHR9+13qzDZfPfU+60hSLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyf2Z/ZqiGuQ==:4ED8\n" +
                "^FT91,4493^A0N,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT271,4493^A0N,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT433,4493^A0N,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^FO30,3906^GFA,337,34200,76,:Z64:eJzs1TEOwzAMBMH8/9NOynQCgbMIgjOdm22Ik58n4/MTSmlpvd6iT/KOWrUWbJTckJaWltapBRslN6SlNaUFGyU3pFVrwUbJDWnVWgA3Jd8vLS0trVOLPsk7amlNadEneUetWgvgpuT7pVVrwUbJDWnVWgA3Jd8vLa0pLdgouSGtWgs2Sm5IS2tKiz7JO2rVWgDskPx3aGlpaZ1asFFyQ1paU1r0Sd5Rq9aCjZIb0qq16JO8o5aWltapBXBT8v3S0prSok/yjlpaU1oANyXfLy0tLa1Tiz7JO/5/fwEAAP//GzWLsFmjYOAANeNx1KxRs4aKWQBA8aW0:CEDE\n" +
                "^FO37,3868^GB576,0,6^FS\n" +
                "^FT127,3809^A0N,100,89^FH\\^CI28^FDJJ3443^FS^CI27\n" +
                "^FT415,3799^A0N,36,30^FH\\^CI28^FD26MAR^FS^CI27\n" +
                "^FT163,3665^A0N,171,205^FH\\^CI28^FDGIG^FS^CI27\n" +
                "^FO37,3418^GB576,0,6^FS\n" +
                "^FT109,3475^A0N,36,30^FH\\^CI28^FDRIO JANEIRO GIG^FS^CI27\n" +
                "^FT91,3151^A0N,36,30^FH\\^CI28^FDDANIEL /GUZMANMR^FS^CI27\n" +
                "^FT91,3196^A0N,36,30^FH\\^CI28^FD3L2J4Z                  1/10KG^FS^CI27\n" +
                "^FT91,3241^A0N,36,30^FH\\^CI28^FDPRINT DATE GMT:  26MAR21^FS^CI27\n" +
                "^FT91,3286^A0N,36,30^FH\\^CI28^FDSAO PAULO^FS^CI27\n" +
                "^FT91,3331^A0N,36,30^FH\\^CI28^FD0957   JJ   047128^FS^CI27\n" +
                "^FT91,3376^A0N,36,30^FH\\^CI28^FD0957047128^FS^CI27\n" +
                "^FO37,3076^GB576,0,6^FS\n" +
                "^FO37,2860^GB576,0,6^FS\n" +
                "^FT199,2458^A0I,36,30^FH\\^CI28^FD26MAR^FS^CI27\n" +
                "^FT484,2582^A0I,171,205^FH\\^CI28^FDGIG^FS^CI27\n" +
                "^FT566,2782^A0I,36,30^FH\\^CI28^FDRIO JANEIRO GIG^FS^CI27\n" +
                "^FT481,2456^A0I,100,89^FH\\^CI28^FDJJ3443^FS^CI27\n" +
                "^FO37,1495^GFA,305,32400,72,:Z64:eJzt1NEJwEAIRMH033TSgXBkEfFmCng/sr5vxJPJ6Oj869AjdS+dugObpHaho6NzTwc2Se1CR2dCBzZJ7UKn7sAmqV3o1B2AU6n/o6Ojc0+HHql76ehM6NAjdS+dugNwKvV/dOoObJLahU7dATiV+j86OhM6sElqFzp1BzZJ7UJHZ0KHHql76dQdAGZK/XkdHZ17OrBJahc6OhM69EjdS6fuwCapXejUHXqk7qWjo3NPB+BU6v/o6Ezo0CN1Lx2dCR2AU6n/o6Ojc0+HHql76dQdeqTupaMzofMBBDjKgQ==:DAEF\n" +
                "^FO37,2410^GB576,0,6^FS\n" +
                "^FO91,793^GFA,233,31260,60,:Z64:eJzty7EJhGAYA9DIFZaO4CY3mWDhYrqJI1haiN/9N4OV8FKEQHi1HXOlq+Vf15jvcKXuZOzvTKm11Zlhqb6OtDfz3tZnb0dSLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyLMuyL7RP8gPDv1cj:A982\n" +
                "^FT185,1376^A0I,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT365,1376^A0I,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT527,1376^A0I,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^FO109,595^GFA,1809,6420,60,:Z64:eJzlVj2P40QYnlkz5HRScIO7i9yio6BKd5KXf7BFXEbiJyzSuk74BfcLEFeeQgHlKQ3+CUuBtEgZEbqrokg0exB5eD/HTrodS1zBaDV27Hnn2ed93ucdG/Nfj09GxL4aEXuXHjo5pcdOd+mx5QjcYvFxcGcfi+8IfUfxvR+B26bHfp4e+v8bY3rOixGxI+rKjfBvNQLXp/t3EtJx5w37d9XFzfZVaPEuBJxO+OgRf5dhnwUce15pg+AuvcROfTvzFOvxEVKyzAseW4+j5ZXOC98qSGwe9iXtbEMnuBnvD//OGe5EcYtaYl197eobjK1RPZz4DpbAGxzXupKfD3HbPByNouHE/4FZhXU+xIUfR8Nk1kZuWuexDzFLnjynZD0d8oUfHFMGiYUbzivPDB4k7xe4ok3RmHhjm1ukQzNJ2NBLmIsGxzfClxZc4DJTZk23Fb5E1tU5bk8TBNsRcVbRE2viO+PfO7lRvlrNkF4UjBJuQ4j7Cy6NE6R6iCtXlBUFY6HrOupIEhb8e2HqM32150A5YRbXJLTgtorL6eWqppU6XolYLQrWUmEvfeRFfFlW/xBX6mDGYB9IMUChoZgWpXGIe7zEle8cB6qBetck6JxkZFHv6CWN27hSxvwu4laYQsSVvBrFzcIgvwNc8ZEFNWdIkTWmYZSv9QNde75O/JsBG8LFvUqC6S5x2wvcSnHBsyQdCupETtXXDnV1qq9V/2ag3gpTiXthKVveVOZOinqIm4fI9wFlvSfXooGlSfWzOjzyXWq/soCLEuKV/omMy0hwA4puznBtiLigXvNtc0vexYlNzPqSgRtxuOrrouJIL3wIjxnjnkDwY4+LXLVLKO5KKo8oWf+7f0CdiSs3LaOteR0PH+FrvY/HUTjZcAynjLtFp9szbhlaq4ePvMh7I4OQIDFNrKrIGFuzNOmoLwpuFLcDyNBNFLeUQ+kkWJJ3xc1illEt4Nsud0wTTKyH0k44WrGv8HV938KDEKxEE+VQy4g3nyDucYibU5Y/5UfNHQjXNKzevME/fkwcm9vYjXkFqAtvimvGDZPQVoF3BVApI8YFsoMyFly4SA6Wnk5eZgNk5UBmvkB22h9DLc2eSNMjhATc15ReSLKUEeNaxG3PcR8NG96ghK6mCSnALHLyBcQt+mNXVpCtbxj3h7AuYWpp1z9FvXhR+0Zc6udMeOa/99SoWL1fpVjjJR7QyndBbZyKvgy/BJj+oO2z8JekUHBX3fCDBOe/t99hT3tHfLEhu6YmWS1amVbKJ5S2axP1Ba748IZxyTzsSRv+4TLKxaNViB9gjPvZ4YCt8/Ce+ZJpWTHLTFzs07PerMK3rvEV5zlH3ImcBobMKW0ZlS5714iPNj9he3v5lmUj04opqRtLW0aJC+3Fqi+/M05xT2e47RA319M39o2vyMBXxJe+Dq3SWqLO+t2xo/J9OONLZzKkhHAngZqylHGF+R3gTvT0jbjYJrdRNmrKoucc5bR87OIjp71Y9aV3WuKEaxW3RN4D3KxvioyLmTmwuiTpfc93hqoO+Nq+O0X/+ph7S4xUxlzAFNfq6TvoG91ms1kbkfQGp4WIuDBDfU38qor+rb9W+z51AK45wEiJnYLeRf+N90Tcvbnabrc/J8S6hrKTRDjvDmTi9wmx099qEiIJd/PjG2O+3LxNiHVi6xRcoHpozVUSX8c5Tiss0Bbs+/xdSiwXlU3CvTqQuF+kxJ596Dx1PAf7vkmMtan2xXFIq2YaxQhcsG+SujRG4EJNJ/M1dgSueZniXhluBK5J5yunb+J4RvO/yMVXHw==:C189\n" +
                "^FT540,496^A0I,36,30^FH\\^CI28^FDDANIEL /GUZMANMR^FS^CI27\n" +
                "^FT540,451^A0I,36,30^FH\\^CI28^FD3L2J4Z                  1/10KG^FS^CI27\n" +
                "^FT540,406^A0I,36,30^FH\\^CI28^FDSAO PAULO^FS^CI27\n" +
                "^FT540,361^A0I,36,30^FH\\^CI28^FDGIG    JJ3443    26MAR^FS^CI27\n" +
                "^FT540,316^A0I,36,30^FH\\^CI28^FD     26MAR21^FS^CI27\n" +
                "^FO97,120^GFA,677,9000,60,:Z64:eJzt1juO3DAMBmAKLlTqBquLBOMcK4Vhe+GLaZCLKEixZZSOhWLuTzs7eTRmqgQBOQOD8OCDJUukRu5tEQqy6aVnuqVOshPluNNEUnBhSptEaYRfaanIhoofiMStW7du3bp169atW7du3bp169atW7du3bp1+1eth8d/HLlk6bESzdSJFtxYU6VUNL2Kpf1kA6olryPT2IKhcISz8JsdpMIunRbW9CK0hH/Yo2ZXVDOGgvQiBt5/t88iwWbbnKWp7fikz5hq6EvgXdOLiHWEP2wSSXe1PG5t1tRgY/3FDi1vdTLaVB527YcdsMJIDTavahk2A9yGhruUbVZHTeHNzn9k99NSzw/7Ug0WaxT6YRv1JwXTwE9YXIMN3AdWi6f1dwo61jditxms9HN+eLV9UsBBxGZRR4Dnt38AwLxlj7Jb7FhRq2oLdVbb1LLJCh/2Pa5dXxBmsOyxLbZ3lVeKn0pa6ZsuDOadZ6ycxaKOYF9KxhDUYuxktFqDzxy/HBYP58MOdURqtFJuh70zxk5d7XUdqd1g1wl2BcDzYZvVfmz4qyFSRPvGKF+xXXg29A30q4Seo5sJFvt4URt0e1z2K+zJJDU2tfvD2vok9kaEradFU1bLtv6M/TzIGova41w4re1cyEVr/7vFIXRa23nk8Q/GK+rqM2g=:31F5\n" +
                "^FT547,80^A0I,28,41^FH\\^CI28^FD0957^FS^CI27\n" +
                "^FT367,80^A0I,28,41^FH\\^CI28^FDJJ^FS^CI27\n" +
                "^FT205,80^A0I,28,41^FH\\^CI28^FD047128^FS^CI27\n" +
                "^PQ1,,,Y\n" +
                "^XZ";
        byte[] bytes = strg.getBytes();
        return bytes;
    }
}
